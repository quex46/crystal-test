const Koa = require('koa');
const Pug = require('koa-pug');
const convert = require('koa-convert');
const serveStatic = require('koa-static');
const i18n = require('i18n');
const app = new Koa();
const router = require('./router');
const server = module.exports = require('http').createServer();
const UPLOAD_DIR = db.model('File').getUploadDir();

i18n.configure({
  locales: ['ru'],
  defaultLocale: 'ru',
  directory: __dirname + '/locales'
});

const pug = new Pug({
  viewPath: __dirname + '/views',
  noCache: process.env !== 'production',
  locals: {
    locale: 'ru',
    __: i18n.__,
    __n: i18n.__n
  }
});

pug.use(app);
app.use(convert(serveStatic(UPLOAD_DIR)));
app.use(router.routes());

server.on('request', app.callback());