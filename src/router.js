const router = module.exports = require('koa-router')();
const convert = require('koa-convert');
const body = convert(require('koa-body')({
  multipart: true,
  formidable: {
    hash: 'md5'  
  }
}));

const User = db.model('User');
const File = db.model('File');

/**
 * Запрещает просмотр страницы авторизованному пользователю
 */
function disallowUser(ctx, next) {
  if (ctx.state.user) {
    if (ctx.request.method === 'POST') {
      ctx.throw(400, 'You Are Logged In');
    } else {
      ctx.redirect( router.url('myfiles') );
    }    
  } else {
    return next();
  }
}

/**
 * Запрещает просмотр страницы гостю
 */
function disallowGuest(ctx, next) {
  if (!ctx.state.user) {
    if (ctx.request.method === 'POST') {
      ctx.throw(403, 'Auth Required');
    } else {
      ctx.redirect( router.url('login') );
    }  
  } else {
    return next();
  }
}


router
  .get('/favicon.ico', (ctx) => ctx.throw(404));

router
  .use((ctx, next) => User.initSession(ctx).finally(next));

router
  .get('index', '/', (ctx) => File
    .handleListRequest(ctx)
    .then((files) => ctx.render('files', { files, title: 'UPLOADED_FILES' }))
  );

router
  .get('myfiles', '/myfiles', disallowGuest, (ctx) => ctx.state.user
    .getFiles()
    .then((files) => ctx.render('files', { files, title: 'MY_FILES' }))
  );

router
  .get('register', '/register', disallowUser, (ctx) => ctx.render('register'))
  .post(router.url('register'), disallowUser, body, (ctx) => User
    .handleRegisterRequest(ctx)
    .then(() => ctx.redirect( router.url('myfiles') ))
    .catch((errors) => ctx.render('register', { errors, form: ctx.request.body }))
  );

router
  .get('login', '/login', disallowUser, (ctx) => ctx.render('login'))
  .post(router.url('login'), '/login', disallowUser, body, (ctx) => User
    .handleAuthRequest(ctx)
    .then(() => ctx.redirect( router.url('myfiles') ))
    .catch((errors) => ctx.render('login', { errors, form: ctx.request.body }))
  );

router
  .get('logout', '/logout', disallowGuest, (ctx) => ctx.render('logout'))
  .post(router.url('logout'), (ctx) => User
    .destroySession(ctx)
    .then(() => ctx.redirect('/login'))
  );

router
  .get('upload', '/upload', disallowGuest, (ctx) => ctx.render('upload', { ok: ctx.request.query.ok }))
  .post(router.url('upload'), disallowGuest, body, (ctx) => File
    .handleUploadRequest(ctx)
    .then((file) => ctx.redirect(router.url('upload') + '?ok=' + encodeURIComponent(file.name)))
    .catch((errors) => ctx.render('upload', { errors, form: ctx.request.body.fields }))
  );