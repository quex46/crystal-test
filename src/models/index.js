module.exports = function (sequelize) {
  const User = sequelize.import('./User');
  const File = sequelize.import('./File');

  File.belongsTo(User);
  User.hasMany(File);
};