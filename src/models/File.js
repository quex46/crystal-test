const bluebird = require('bluebird');
const filesize = require('filesize');
const dateformat = require('dateformat');
const _ = require('underscore');
const md5file = require('md5-file');
const fs = require('fs-extra');
const path = require('path');
const UPLOAD_DIR = path.join(__dirname, '../../upload');

module.exports = function (sequelize, DataTypes) {
  
  const attributes = {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    privacy: {
      type: DataTypes.ENUM,
      allowNull: false,
      values: ['public', 'private'],
      defaultValue: 'public'
    },
    size: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    size_human: {
      type: DataTypes.VIRTUAL,
      get: function () {
        return filesize(this.size);
      }
    },
    created_at_human: {
      type: DataTypes.VIRTUAL,
      get: function () {
        return dateformat(this.created_at, 'dd/mm/yyyy, H:MM:ss');
      }
    }
  };

  const options = {
    tableName: 'files',
    underscored: true,
    
    classMethods: {
      /**
       * Возвращает базовую директорию для загрузки файлов
       * @return {string}
       */
      getUploadDir: () => UPLOAD_DIR,
      
      /**
       * Перемещает загруженный файл в папку загрузок
       * @param  {object} file Элемент ctx.request.body.files
       * @return {string}      Относительный путь до файла
       */
      moveUploadedFile: (file) => bluebird
        .resolve(file.hash || md5file(file.path))
        .then((hash) => {
          var dir = path.join(UPLOAD_DIR, hash.substr(0, 2), hash.substr(2, 2));
          var savePath = path.join(dir, hash + path.extname(file.name));
          return bluebird.promisify(fs.mkdirp)(dir).return(savePath);
        })
        .tap((savePath) => bluebird.fromCallback(
          (cb) => fs.move(file.path, savePath, { clobber: true }, cb)
        ))
        .then((savePath) => '/' + path.relative(UPLOAD_DIR, savePath)),

      /**
       * Обработка запроса на загрузку файла
       * @param  {object} ctx Контекст из koa
       * @return {File}
       */
      handleUploadRequest: function (ctx) {
        var fields = ctx.request.body.fields;
        var file = ctx.request.body.files.file;

        if (!file || !file.size) {
          return bluebird.reject(['E_EMPTY_FILE']);
        }

        var data = {
          name: file.name || 'Untitled File',
          privacy: _.isUndefined(fields.private) ? 'public' : 'private',
          size: file.size || null
        };

        return this
          .moveUploadedFile(file)
          .then((relPath) => {
            data.path = relPath;
            return this.create(data);
          })
          .tap((file) => file.setUser(ctx.state.user))
          .catch((err) => {
            var errors = [];
            switch (err.name) {
              default:
                console.log(err); // TODO: лог в файл
                errors.push('E_UNKNOWN_ERROR');
                break;
            }
            throw errors;
          });
      },

      /**
       * Загрузка списка файлов
       * TODO: пагинация
       * @param  {object} ctx Контекст из koa
       * @return {array}      Список файлов
       */
      handleListRequest: function (ctx) {
        var query = {
          where: {},
          order: 'id DESC',
          include: [sequelize.model('User')]
        };
        
        if (!ctx.state.user) {
          query.where.privacy = 'public';
        }

        return this.findAll(query);
      }
    }
  };

  return sequelize.define('File', attributes, options);
};