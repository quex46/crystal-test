const crypto = require('crypto');
const bluebird = require('bluebird');
const _ = require('underscore');

module.exports = function (sequelize, DataTypes) {
  const attributes = {
    login: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        is: {
          args: [/^[a-z0-9_]+$/],
          msg: 'E_INVALID_LOGIN_CHARS'
        },
        len: {
          args: [3, 16],
          msg: 'E_INVALID_LOGIN_LENGTH'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      set: function (pass) {
        if (pass.length < 5 || pass.length > 64) {
          let errItem = new sequelize.ValidationErrorItem(
            'E_INVALID_PASSWORD_LENGTH',
            'ValidationError',
            'password',
            pass
          );
          let err = new sequelize.ValidationError('Validation Error', [errItem]);         
          
          throw err;
        }

        let salt = this.getDataValue('salt');
        let passHash = this.Model.hashPassword(pass, salt);
        this.setDataValue('password', passHash);
      }
    },
    salt: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: () => sequelize.model('User').createSalt()
    }
  };  
  const options = {
    tableName: 'users',
    underscored: true,
    
    instanceMethods: {
      validatePassword: function (pass) {
        return this.password === this.Model.hashPassword(pass, this.salt);
      },
      setSessionCookies: function (ctx) {
        let cookies = ctx.cookies;
        cookies.set('uid', this.id);
        cookies.set('password', this.password);
      }
    },
    
    classMethods: {
      /**
       * Генерация соли для подмешивания в хэш пароля
       * @return {string} Соль
       */
      createSalt: () => crypto
        .randomBytes(16)
        .toString('base64'),     
      
      /**
       * Генерация соленого хэша пароля
       * @param  {string} pass Пароль
       * @param  {string} salt Соль
       * @return {string}      Соленый хэш пароля
       */
      hashPassword: (pass, salt) => crypto
        .createHash('sha256')
        .update(pass + ':' + salt)
        .digest('hex'),
      
      /**
       * Обработка запроса на авторизацию
       * @param  {object} ctx   Контекст из koa
       * @return {User}         Пользователь
       */
      handleAuthRequest: function (ctx) {
        var body = ctx.request.body;
        var login = body.login;
        var password = body.password;
        
        return bluebird
          .delay(1000) // Снижаем эффективность брутфорса
          .then(() => this.findOne({ where: { login } }))
          .tap((user) => {
            if (!user || !user.validatePassword(password)) {
              let err = new Error('E_USER_NOT_FOUND');
              err.name = 'UnknownUser';
              throw err;
            }

            user.setSessionCookies(ctx);
          })
          .catch((err) => {
            var errors = [];
            switch (err.name) {
              case 'UnknownUser':
                errors.push(err.message);
                break;
              default:
                console.log(err); // TODO: лог в файл
                errors.push('E_UNKNOWN_ERROR');
                break;
            }
            throw errors;
          });
      },

      /**
       * Обработка запроса на регистрацию пользователя
       * @param  {object} ctx   Контекст из koa
       * @return {User}         Зарегистрированный пользователь
       */
      handleRegisterRequest: function (ctx) {
        var fields = ['login', 'password', 'password_confirm'];
        var data = _.pick(ctx.request.body, fields);

        if (data.password !== data.password_confirm) {
          return bluebird.reject(['E_PASSWORD_CONFIRM_MISMATCH']);
        }

        return bluebird
          .delay(1000) // Небольшая защита от спама регами
          .then(() => this.create(data))
          .tap((user) => user.setSessionCookies(ctx))
          .catch((err) => {
            var errors = [];
          
            switch (err.name) {
              case 'SequelizeValidationError':
                errors = _.map(err.errors, (err) => err.message);
                break;
              case 'SequelizeUniqueConstraintError':
                errors.push('E_LOGIN_IN_USE');
                break;
              default:
                console.log(err); // TODO: лог в файл
                errors.push('E_UNKNOWN_ERROR');
                break;
            }

            throw errors;
          });
      },

      /**
       * Инициализирует сессию в Koa (ctx.state.user)
       * @param  {object} ctx Контекст из koa
       * @return {User}       Пользователь
       */
      initSession: function (ctx) {
        let where = {
          id: ctx.cookies.get('uid'),
          password: ctx.cookies.get('password')
        };

        if (!where.id || !where.password) {
          return bluebird.resolve(null);
        }

        return this
          .findOne({ where })
          .tap((user) => {
            if (user) {
              ctx.state.user = user;
            } else {
              this.destroySession(ctx);  
            }                      
          })
          .catch((err) => {
            console.log(err);
            this.destroySession(ctx);
          });
      },

      /**
       * Очистка сессии
       * @param  {object} ctx  Контекст koa
       * @return {Promise}     Пустой промис
       */
      destroySession: (ctx) => {
        let cookies = ctx.cookies;
        
        cookies.set('uid', '');
        cookies.set('password', '');
        
        return bluebird.resolve();
      }
    }
  };

  return sequelize.define('User', attributes, options);
};