# Задача

## Написать web­приложение, которое реализует следующий функционал:

### 1) Авторизация пользователя

### 2) Добавление файлов, только авторизированным пользователем (файлы могут быть
двух типов: "для всех" и "только для авторизированных пользователей")

### 3) Просмотр списка доступных файлов:
– НЕ авторизированным пользователя отображаются файлы "для всех"
– Авторизированным пользователям отображаются файлы "для всех" и "только для
авторизированных пользователей"

## Используемые технологии

### Серверная часть:

– Node.js, желательно использовать koa2 и jade(pug)
– Postgresql

### Клиентская часть:
– Главное, чтобы можно было посмотреть результат серверной части