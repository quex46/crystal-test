FROM node:slim

COPY package.json index.js config.json /var/www/
COPY src /var/www/src/

RUN cd /var/www && npm install

CMD ["node", "/var/www/index.js"]