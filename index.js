const bluebird = require('bluebird');
const Sequelize = require('sequelize');
const config = require('./config');

var seq = global.db = new Sequelize(
  config.db_name, 
  config.db_user, 
  config.db_password, 
  {
    host: config.db_host,
    port: config.db_port,
    dialect: 'postgres',
    pool: {
      max: 10,
      min: 2,
      idle: 10000
    }
  }
);

seq.import(__dirname + '/src/models');

module.exports = () => bluebird
  .all([
    seq.model('User').sync(),
    seq.model('File').sync()
  ])
  .then(() => {
    // Ленивая загрузка, т.к. в этом модуле уже идет работа с моделями.
    var server = require('./src/server'); 
    return bluebird.fromCallback((cb) => server.listen(config.listen_port, cb));
  });

if (!module.parent) {
  module.exports();
  
  // Иначе докер не реагирует на Ctrl+C
  process.on('SIGTERM', () => process.exit()); 
  process.on('SIGINT', () => process.exit());
}



